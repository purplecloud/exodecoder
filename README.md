# Exodecoder

## Algorithm design

Below is a high level explanation of the steps followed by the algorithm to solve the exodecoder problem.

The algorithm accepts a sequence of numeric characters, such as “123”.
For such sequence, we will extract every possible encoding values.
For the above sequence, this will be the resulting set of possible encoding values:

 1, 2, 3, 12, 23

For each of such elements, a simple ETL will be applied.
Such ETL will ber responsible for:

1.  Validate the correct typing of the element and return an exception if not compliant
2.  Verify that each element can be converted to an integer smaller than 27
3.  For each element, it will return a three dimensions tuple that contains: a) the position of the encoding value in the input sequence; b) the encoding value itself; c) the position of the next encoding value in the input sequence

This is an example of what the ETL would return for some encoding values:

1 -> (0, 1, 1)
2 -> (1, 2, 3)
12 -> (0, 12, 2)

After all the encoding values are mapped to this tuples, a tree is built by treating all the previous tuples as nodes.

By doing this, we will end up by with a tree whose every path represents a viable set of combinations that can be used to decode the single encoding values to letters of the final output.

Every node of such tree, is represented by a tuple whose 1st element is the encoding value tuple created by the provious ETL, and the 2nd element is the parent node.

The traversed path for the combination of encoding values 1 2 3 for example, will be represented by such tuple.

((2, 3, 3), ((1, 2, 2), ((0, 1, 1), ((-1, -1, 0), None))))

Note that every tree path is traversed bottom-up, and the ((-1, -1, 0), None) node represents the root of the tree.

A more abstract representation of such tree of encoding values, will be:

        0
       / \
      1  12
     / \  \
    /   23  3
    3

After every traversable tree path is computed, for every path the root node is removed and the rest of the path is mapped to just the encoding values

The tree path for the combination of encoding values 1 2 3 will then become:

[1, 2, 3]

As a last step, all the mapped tree paths will be decode to single letters and the characters will be joined in strings. The previous travesed tree path, will then become:

‘ABC’

The whole output of applying the above algorithm the the input sequenc '123' will be:

['LC' , 'AW', 'ABC']


### How to run

Clone the repo and get into the folder:

```bash
git clone https://purplecloud@bitbucket.org/purplecloud/exodecoder.git
cd exodecoder
```

#### Run
```bash
python main.py 123
```

#### Run the tests

```bash
pip install -U pytest
pytest
```

### Bugs and limitation
There are no known bugs or limitations about the input the algorithm can handle.
If the user input non valid characters (everything except number) the program will throw an exception.

#### Edge cases
If the input sequence contains 0s, the program will still work. However, because is not a valid encoding value per se, one or more output combinations will contain the string 0! in it to notify the user that is loosing a combination because it contains an invalid encoding value.

Example:

```bash
python main.py 10
['J', 'A0!']
```
