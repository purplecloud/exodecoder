import main


def test_alphabet():
    assert len(main.alphabet()) == 26


def test_etl():
    assert main.etl(0, '2') == (0, 2, 1)
    assert main.etl(1, '27') == (-1, -1, -1)


def test_combinations():
    list = []
    for x in main.get_combinations("1233"):
        list.append(x)

    assert len(list) == 6


def test_attach_node_to_tree():
    tree = [((0, 11, 2), None)]
    tree = main.attach(tree, (2, 4, 3))
    assert tree == [((0, 11, 2), None), ((2, 4, 3), ((0, 11, 2), None))]


def test_trim_tree():
    tree = [((0, 11, 2), None)]
    tree = main.trim(tree, 2)
    assert tree == tree
    tree = main.trim(tree, 3)
    assert tree == []


def test_tree_mapping():
    mapped_tree = []
    traversed_tree = ((1, 2, 2), ((0, 1, 1), ((-1, -1, 0), None)))
    main.map(traversed_tree, mapped_tree)
    assert mapped_tree == [1, 2]


def test_build_tree():
    tree = main.build_tree('12')
    assert tree == [
        ((0, 12, 2), ((-1, -1, 0), None)),
        ((1, 2, 2), ((0, 1, 1), ((-1, -1, 0), None)))]


def test_decoding():
    out = []
    main.decode(1, out)
    main.decode(26, out)
    assert out == ['A', 'Z']
