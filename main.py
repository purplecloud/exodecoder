import sys
import string


# Return the english alphabet as a list of characters
def alphabet():
    return list(string.ascii_lowercase)


'''
Simple ETL that validates if a character is a number smaller that 26,
and returns a Tuple with the following structure:
d[0] = the position of the numeric character in the input list
d[1] = the numeric character itself as an integer
d[2] = the position of the next numeric character in the input list
'''


def etl(offset, text):
    try:
        nmbr = int(text)
        if nmbr > len(alphabet()):
            return (-1, -1, -1)
        else:
            return (offset, nmbr, len(text)+offset)
    except ValueError:
        print("Only numeric characters are allowed")


'''
Generator that returns all the valid combinations of the input list's elements
as a custome data structure (see the etl function)
'''


def combinations(data, offset):
    if offset < len(data):
        for i in range(2):
            if offset+i < len(data):
                yield etl(offset, data[offset:offset+i+1])
        for c in combinations(data, offset+1):
            yield c


'''
Discards invalid combinations of numbers (ex. numbers bigger thatn 26)
'''


def get_combinations(input):
    for c in combinations(input, 0):
        if c[1] != -1:
            yield c


'''
Attach a node to the tree
'''


def attach(tree, node):
    t = []
    for link in tree:
        t.append(link)
        if link[0][2] == node[0]:
            t.append((node, link))
    return t


'''
Removes all the incompletely traversed branches of the tree
'''


def trim(tree, end):
    t = []
    for node in tree:
        if node[0][2] == end:
            t.append(node)
    return t


'''
Map a traversed branch of the tree to a list of numbers, where such
numberes are the values of the nodes
'''


def map(traverse, encoded):
    encoded.insert(0, traverse[0][1])
    if traverse[1][1] is not None:
        return map(traverse[1], encoded)
    return traverse


'''
Build a tree where nodes are a tuple, and links are the parent node
'''


def build_tree(input):
    root = ((-1, -1, 0), None)
    tree = [root]
    for c in get_combinations(input):
        tree = attach(tree, c)
    return trim(tree, len(input))


'''
Apply the mapping function (see map) to every traversed branch of the tree
'''


def map_tree(tree):
    for traverse in tree:
        mapped_tree = []
        map(traverse, mapped_tree)
        yield mapped_tree


'''
Decode a number to its corresponding character of the
english alphabet (ex. 1 -> A)
'''


def decode(code, values):
    values.append("0!" if code == 0 else (alphabet()[code-1]).upper())


'''
Build a string out of a list of characters
'''


def join(chars_sets):
    strings = []
    for chars in chars_sets:
        strings.append(string.join(chars))
    return strings


'''
Entrypoint that axcepts a sequence of numbers as input and output a list of
decoded characters as output
'''


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Enter a valid string like '123'")
    else:
        chars_sets = []
        tree = build_tree(sys.argv[1])
        for encoded_chars in map_tree(tree):
            decoded_chars = []
            for code in encoded_chars:
                decode(code, decoded_chars)
            chars_sets.append(decoded_chars)
        print(join(chars_sets))
